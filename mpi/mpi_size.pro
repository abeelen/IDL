;+
; NAME:
;      mpi_size
;
;
; PURPOSE:
;      Retrieve mpi_size and/or default value to 1
;
;
; CATEGORY:
;      MPI Parallelization
;
;
; CALLING SEQUENCE:
;    mpi_size, size
;
;
; INPUTS:
;
;
;
; OPTIONAL INPUTS:
;
;
;
; KEYWORD PARAMETERS:
;
;
;
; OUTPUTS:
;    size : the actual MPI_COMM_WORLD_SIZE
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;       The system variables used here, are defined for the OpenMPI
;       implementation but could be easily translated to other MPI
;       implementations
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;
;-

PRO mpi_size, size

  size = GETENV('OMPI_COMM_WORLD_SIZE')
  IF size EQ '' THEN size= 1

END
