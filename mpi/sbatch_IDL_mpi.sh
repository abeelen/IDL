#!/bin/bash

#SBATCH -N 2
#SBATCH --job-name "MPI IDL POC"

echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NPROCS processors."
echo "nb procs $SLURM_JOB_CPUS_PER_NODE"

mpirun idl mpi_poc.pro
