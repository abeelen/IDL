;+
; NAME:
;       MPI_HELPER [MPI_TO|MPI_FROM]
;
;
; PURPOSE:
;       Compute the starting and ending [ starting or ending] loop
;       index for an MPI based parallelization
;
;
; CATEGORY:
;       MPI Parallelization
;
;
; CALLING SEQUENCE:
;      Replace a given loop
;
;      FOR I=from, to-1 DO $
;          PRINT, I
;
;      by
;
;      mpi_helper, from, to, i_from, i_to
;      FOR I=i_from, i_to-1 DO $
;          PRINT, I
;
;      or
;
;      FOR I=MPI_FROM(from, to), MPI_TO(from, to)-1 DO $
;          PRINT, I
;
;      such that the loops are split between all MPI processes.
;
;
; INPUTS:
;      from : the starting point of the FOR loop
;      to   : the ending point of the FOR loop
;
;
; OPTIONAL INPUTS:
;
;
;
; KEYWORD PARAMETERS:
;
;
;
; OUTPUTS:
;      the local start and end points of the given MPI process
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;       all IDL instances are independant, as such it must write date
;       independantly from the others, using, for e.g.,
;       mpi_rank, rank
;       as an index
;
;
; RESTRICTIONS:
; 
;       See mpi_rank/mpi_size for restriction
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;
;-


PRO MPI_HELPER, from, to, start_this, stop_this

  mpi_rank, rank
  mpi_size, size
  
  count = (to-from)/size
  remainder = (to-from) MOD size
  
  start_this = rank * count + min([rank, remainder])
  stop_this = (rank + 1) * count + min([rank + 1, remainder])
  
  start_this += from
  stop_this  += from

END

FUNCTION MPI_FROM, from, to
  
  mpi_rank, rank
  mpi_size, size
    
  count = (to-from)/size
  remainder = (to-from) MOD size
  
  start_this = rank * count + min([rank, remainder])
  
  start_this += from

  RETURN, start_this
  
END

FUNCTION MPI_TO, from, to
  
  mpi_rank, rank
  mpi_size, size
    
  count = (to-from)/size
  remainder = (to-from) MOD size
  
  stop_this = (rank + 1) * count + min([rank + 1, remainder])
  
  stop_this  += from

  RETURN, stop_this
END

