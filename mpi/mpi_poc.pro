;; Proof of concept to run IDL with MPI to run on several core and/or
;; node for overly simple loop parallelization without message
;; passing.
;; 
;; launch idl with
;; > mpirun -n #tasks idl idl_mpi_poc.pro

;; Retrieve the MPI RANK & SIZE from system variables
mpi_rank, rank
mpi_size, size


;; Let's compute the sum of all indexes between start and stop
start = 0
stop = 11
sum = 0

mpi_helper, start, stop, i_start, i_stop

;; Check the local min max
print, rank, size, i_start, i_stop, $
       FORMAT='("rank: ",I2," / ",I2," will do ",I2," - ",I2)'

;; wait for all screen outputs
WAIT, 2

;; This is the main loop, should be an intensive task....
FOR index=i_start, i_stop-1 DO BEGIN $
   PRINT, rank, index, FORMAT='("rank: ", I2, " doing ", I2)' &$
   WAIT, i_start &$
   sum += index

;; Save the output in an unique output file
SAVE, FILENAME=STRING(rank, FORMAT='("poc_",I2.2,".sav")'), sum

PRINT, rank, FORMAT='("rank: ", I2, " done")'

;; Do not forget the exit at the end
exit

;; The following can be used to combine the data
sum_combined = 0
files = FILE_SEARCH('poc_*.sav')
FOR iFile=0, N_ELEMENTS(files)-1 DO BEGIN $
   RESTORE, files[iFile] &$
   sum_combined += sum 

PRINT, sum_combined
