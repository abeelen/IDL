;+
; NAME:
;      mpi_rank
;
;
; PURPOSE:
;      Retrieve mpi_rank and/or default value to 0
;
;
; CATEGORY:
;      MPI Parallelization
;
;
; CALLING SEQUENCE:
;    mpi_rank, rank
;
;
; INPUTS:
;
;
;
; OPTIONAL INPUTS:
;
;
;
; KEYWORD PARAMETERS:
;
;
;
; OUTPUTS:
;    rank : the actual MPI_COMM_WORLD_RANK
;
;
; OPTIONAL OUTPUTS:
;
;
;
; COMMON BLOCKS:
;
;
;
; SIDE EFFECTS:
;
;
;
; RESTRICTIONS:
;
;       The system variables used here, are defined for the OpenMPI
;       implementation but could be easily translated to other MPI
;       implementations
;
;
; PROCEDURE:
;
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;
;-

PRO mpi_rank, rank

  rank = GETENV('OMPI_COMM_WORLD_RANK')
  IF rank EQ '' THEN rank = 0

END
