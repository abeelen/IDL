[IDL] & [MPI]
-------------

It is possible to launch several instance of [IDL] using [MPI] :

```bash
nMPI=#ntasks
mpirun -n $nMPI idl mpi_poc.pro
```

will launch several independent [IDL] processes, which will all run the same program `mpi_poc.pro`. This would produce several times the exact same output. However, at least using the [OpenMPI](https://www.open-mpi.org/) implementation, it is possible to retrieve `MPI_COMM_WORLD_RANK` and `MPI_COMM_WORLD_SIZE` as system variables and thus split any big [FOR loop]. The results will then depends on the `MPI_COMM_WORLD_RANK` value and could be saved using the rank as an unique identifier.

I wrote a few [simple scripts](https://git.ias.u-psud.fr/abeelen/IDL/tree/master/mpi) to help parallelize a long [FOR loop]. Note however that combining the results of the [FOR loop] will require disk IO and thus, the iterations of the [FOR loop] should be rather CPU intensive in order to be efficient. The following example compute the sum of all indexes between two values.

```IDL

;; mpi_poc.pro
;; Let's compute the sum of all indexes between start and stop

;; Retrieve the MPI RANK from system variables
mpi_rank, rank

start = 0
stop = 11
sum = 0

;; Divide the task among the different MPI ranks
mpi_helper, start, stop, i_start, i_stop

;; This is the main loop, should be an intensive task....
FOR index=i_start, i_stop-1 DO BEGIN $
   PRINT, rank, index, FORMAT='("rank: ", I2, " doing ", I2)' &$
   WAIT, start &$
   sum += index

;; save the result in an unique output file
SAVE, FILENAME=STRING(rank, FORMAT='("poc_",I2.2,".sav")'), sum

;; Do not forget to exit at the end
exit
```

Launching 5 instances of this program with
```bash
mpirun -n 5 idl mpi_poc.pro
```

will result in 5  files
```bash
poc_00.sav  poc_01.sav  poc_02.sav  poc_03.sav  poc_04.sav
```

which can be combined using
```IDL

sum_combined = 0
files = FILE_SEARCH('poc_*.sav')
FOR iFile=0, N_ELEMENTS(files)-1 DO BEGIN $
   RESTORE, files[iFile] &$
   sum_combined += sum

PRINT, sum_combined
PRINT, TOTAL(INDGEN(11))
```


[IDL]: http://www.harrisgeospatial.com/ProductsandSolutions/GeospatialProducts/IDL.aspx
[MPI]: (https://en.wikipedia.org/wiki/Message_Passing_Interface)
