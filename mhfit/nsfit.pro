PRO nsfit_dummy
  ;; Enclose in a procedure so these are not defined in the main level
  FORWARD_FUNCTION LogSumExp, mhfit, mpfit, nsfit
  
  ;; To be sure on the the first run...
  RESOLVE_ROUTINE, 'mpfit', /IS_FUNCTION, /COMPILE_FULL_FILE
  RESOLVE_ROUTINE, 'mhfit', /IS_FUNCTION, /COMPILE_FULL_FILE
END

FUNCTION LogSumExp, values
  ;; Return the numerically stable log of the sum of an array of log values
  biggest = MAX(values)
  x = values-biggest
  RETURN, ALOG(TOTAL(EXP(x)))+biggest
END


PRO NSFIT_STAT, logLikelihood_chain, N_particules, $
                logMass=logMass, logLikelihood=logLikelihood, logZ=logZ, $
                information=information, effSampleSize=effSampleSize, relWeights=relWeights, $
                doPlot=doPlot, doPrint=doPrint, $
                MSG_PREFIX=msg_prefix

  IF NOT KEYWORD_SET(msg_prefix) THEN msg_prefix = ''

  logLikelihood_keep = logLikelihood_chain[WHERE(FINITE(logLikelihood_chain) EQ 1, N_nested)]
  
  logMass = -(DINDGEN(N_nested)+1)/N_particules

  ;; Normalized Prior weights
  logWeights = logMass - LogSumExp(logMass)

  logLikelihood = logWeights+logLikelihood_keep

  ;; Marginal likelihood
  logZ = LogSumExp(logLikelihood)

  ;; relative posterior weights
  relWeights = EXP(logLikelihood - MAX(logLikelihood))

  ;; normalized posterior weights
  normWeights = relWeights/TOTAL(relWeights)
  
  ;; Compute information
  information = TOTAL(normWeights*(loglikelihood_keep-logZ))

  ;; Compute the effective sample size
  effSampleSize = CEIL(EXP(-TOTAL(normWeights*ALOG(normWeights+1d-300))))
  
  
  IF KEYWORD_SET(doPlot) THEN BEGIN 
     erase
     multiplot, [1,2], MXTITLE="log(X)"
     plot, logMass, logLikelihood_keep, YTITLE="log(Likelihood)"
     multiplot
     plot, logMass, relWeights, YTITLE="relative Posterior weights"
     multiplot,/default
  ENDIF

  IF KEYWORD_SET(doPRINT) THEN BEGIN
     PRINT, STRING(msg_prefix, logZ, SQRT(information/N_particules), $
                   FORMAT='(A," logZ = ", F10.4, " +- ", F10.4)')
     PRINT, STRING(msg_prefix, information, $
                   FORMAT='(A, " Information = ", F10.4)')
     PRINT, STRING(msg_prefix, effSampleSize, $
                   FORMAT='(A, "Effective sample size = ", F10.4)')
  ENDIF

  
END

FUNCTION NSFIT_POSTERIOR, x_keep, relWeights, effSampleSize
  ;; Draw a posterior sample

  sz = SIZE(x_keep)
  n_param = sz[1]
  N_nested = sz[2]

  probability = relWeights/MAX(relWeights)
  posterior_sample = DBLARR(n_param, effSampleSize)


  iSample = 0
  WHILE ( iSample LT effSampleSize)  DO BEGIN
     
     index = FLOOR(RANDOMU(seed)*N_nested)
     prob = probability[index]
     IF RANDOMU(seed) LE prob THEN BEGIN
        posterior_sample[*,iSample] = x_keep[*,index]
        iSample += 1
     ENDIF
  ENDWHILE
  
  RETURN, posterior_sample
END


FUNCTION NSFIT,fcn, particules, parinfo=parinfo, FUNCTARGS=fcnargs, $
               MAXITER=maxiter, TOTITER=TOTITER, $
               STATUS=status, ERRMSG=errmsg, $
               incovar=incovar, NESTED_FACTOR=nested_factor, RECOVAR=recovar, SCCOVAR=SCCOVAR, DZ_MIN=dz_min, $
               information=information, posterior_sample=posterior_sample, logMass=logMass, $
               logLikelihood_keep=logLikelihood_keep, x_keep=x_keep, relWeights=relWeights, $
               doPLOT=doplot, doPRINT=doprint, VERBOSE=verbose, $
               SAVE_NSSTEP=save_nsstep, RESTORE_NSSTEP=restore_nsstep, $
               NPRINT=nprint, MSG_PREFIX=msg_prefix, $
               _EXTRA=_extra

  ;; Perform a nested sampling to recover the evidence of a model.
  ;; See http://www.inference.phy.cam.ac.uk/bayesys/nest.pdf
  ;; See also Kyle Barbary code at https://github.com/AstroHackWeek/AstroHackWeek2015/blob/master/day4-day5-inference/nested_sampling_code/nested_sampling.py

  ;; Follows the definition of mhfit, for the argument

  ; ADDITIONNAL INPUTS:
  ;     particules         - set of input parameter drawn from the priors
  ;     nested_factor      - the multiplicative factor to define the number of nested loop from
  ;                          the size of the particules array
  ;     doPlot             - show the likelihood and weights being built
  ;     doPrint            - print the actual likelihood/information
  ;     logZ               - the actual likelihood ...
  ;     information        - and associated information
  ;     posterior_sample   - parameters sampled from the posterior
  ;     logMass            - the logMass used in the nested sampling with ...
  ;     logLikelihood_keep - the associated likelihoods ...
  ;     relWeights         - and relative weights
  ;     _extra             - Refers to the keywords of MHFIT 


  IF N_ELEMENTS(maxiter)  EQ 0 THEN maxiter  = 20L
  IF N_ELEMENTS(totiter)  EQ 0 THEN totiter  = !VALUES.D_INFINITY

  IF NOT KEYWORD_SET(msg_prefix) THEN msg_prefix = ''
  
  IF NOT KEYWORD_SET(nested_factor) THEN nested_factor=10L ;; this will probe masses from e(-15) to 1.
  IF NOT KEYWORD_SET(nprint) THEN nprint = 10L
  IF NOT KEYWORD_SET(dz_min) THEN  dz_min = 0.
  IF KEYWORD_SET(recovar) AND KEYWORD_SET(sccovar) THEN $
     MESSAGE,msg_prefix+"You must use either RECOVAR or SCCOVAR, but not both", LEVEL=-1
  
  common mpfit_config, mpconfig
  common mpfit_machar, machvals

  mpfit_setmachar, double=1
  MACHEP0 = machvals.machep
  DWARF   = machvals.minnum

  status = 1L
  errmsg = ''
  
  ;; Be sure that PARINFO is of the right type
  IF N_ELEMENTS(parinfo) GT 0 THEN BEGIN
     parinfo_size = size(parinfo)
     IF parinfo_size[parinfo_size[0]+1] NE 8 THEN BEGIN
        errmsg = 'ERROR: PARINFO must be a structure.'
        goto, TERMINATE
     ENDIF
  ENDIF

  xnew = mhfit_parsetup(parinfo, particules[0,*], nfree=nfree, qulim=qulim, ulim=ulim, qllim=qllim, llim=llim, ifree=ifree, errmsg=errmsg)
  IF errmsg NE '' THEN goto, TERMINATE

  sz = SIZE(particules)
  n_param = sz[1]
  N_particules = sz[2]
  
  N_nested = N_particules*nested_factor
    
  IF KEYWORD_SET(VERBOSE) THEN $
     MESSAGE,/INFORMATIONAL, msg_prefix+"Starting..."
  
  x_keep = DBLARR(n_param,N_nested)*!VALUES.F_NAN
  logLikelihood_keep = DBLARR(N_nested)*!VALUES.F_NAN

  iNest = 0L
  
  startTime = systime(/julian)

  IF KEYWORD_SET(save_nsstep) THEN BEGIN
     IF TYPENAME(save_nsstep) EQ "STRING" THEN BEGIN
        save_prefix=save_nsstep
     ENDIF ELSE BEGIN
        save_prefix=date_conv( startTime, 'FITS')
     ENDELSE
  ENDIF
     
  IF KEYWORD_SET(restore_nsstep) THEN BEGIN
     restore_filename = restore_nsstep+'_ns.dat'
     IF FILE_TEST(restore_filename) THEN BEGIN
        RESTORE, restore_nsstep+'_ns.dat'
     ENDIF ELSE BEGIN
        MESSAGE, /INFORMATIONAL, msg_prefix+"restore file "+restore_filename+" doest not exist, continue..."
     ENDELSE
  ENDIF
  
  logLikelihood_particules = DBLARR(N_particules)
  FOR iSample=0, N_particules-1 DO $
     logLikelihood_particules[iSample] = logLikelihood( $
     mpfit_call(fcn, particules[*,iSample], _EXTRA = fcnargs) )

  IF KEYWORD_SET(VERBOSE) THEN $
     MESSAGE,/INFORMATIONAL, msg_prefix+"Computed likelihood"
  

  index_max = (WHERE(logLikelihood_particules EQ MAX(logLikelihood_particules)))[0]
  index_min = (WHERE(logLikelihood_particules EQ MIN(logLikelihood_particules)))[0]
  
  dZ = ABS(logLikelihood_particules[index_max]*1)
  
  WHILE( iNest LT N_nested AND dZ GT dZ_min) DO BEGIN

     ;; FOR iNest=0L, N_nested-1 DO BEGIN

     ;; print, iNest, iNest*100.0/N_nested, FORMAT='(I," ", F, " %")'
     ;; print, iNest, "Acceptance ratio : ", N_ELEMENTS(accept)/accept[N_ELEMENTS(accept)-1]
     index_min = (WHERE(logLikelihood_particules EQ MIN(logLikelihood_particules)))[0]

     lnl_min = logLikelihood_particules[index_min]
     
     ;; keep the minimum
     x_keep[*,iNest] = particules[*,index_min]
     logLikelihood_keep[iNest] = lnl_min
     
     ;; make it evolve with the constraint of higher likelihood
     ;; first pick a starting point with higher likelihood
     REPEAT index_other = FLOOR(RANDOMU(seed)*N_particules) UNTIL $
                          index_other EQ index_min

     ;; make this point evolve with mcmc under the minimum likelihood constraint
     dummy = mhfit(fcn, particules[*, index_other], incovar=incovar, functargs = fcnargs, $
                   maxiter=maxiter, totiter=totiter, status=status, $
                   chains=chains, lnl=lnl, minLogLikelihood=lnl_min, covar=outcovar, accept=accept, $
                   _EXTRA=_extra, /QUIET)

     IF status EQ -1 THEN BEGIN
        errmsg = "MHFIT stalled... exciting..."
        MESSAGE, msg_prefix+errmsg,/INFORMATIONAL
        GOTO, TERMINATE
     ENDIF
     
     IF status EQ 5 THEN BEGIN
        errmsg = "Maximum steps reached in MHFIT..."
        MESSAGE, msg_prefix+errmsg, /INFORMATIONAL
        good_Lnl = WHERE(FINITE(Lnl) EQ 1)
        LnL = LnL[good_Lnl]
        chains = chains[*,good_Lnl]
     ENDIF
     
     IF KEYWORD_SET(recovar) THEN BEGIN
        ;; Check for fixed values and only keep diagonal values...
        nParam = N_ELEMENTS(outcovar[0,*])
        incovar = DBLARR(nParam, nParam)
        index = LINDGEN(nParam)
        incovar[index, index] = outcovar[index, index] > 1d-12
     ENDIF
     IF KEYWORD_SET(sccovar) THEN BEGIN
        ;; Following https://arxiv.org/pdf/0704.3704v3.pdf
        Na = N_ELEMENTS(accept)
;;        accept_rate = N_ELEMENTS(accept)/accept[N_ELEMENTS(accept)-1]
        Nr = accept[Na-1]-Na
        IF Na/Nr GT 1 THEN BEGIN
           incovar = incovar * exp(1./Na)^2
        ENDIF ELSE BEGIN
           incovar = incovar * exp(-1./Nr)^2
        ENDELSE
     ENDIF
     
     ;; Get the last point on the chain, must be uncorrelated from the
     ;; first one
     length = (size(chains))[2]
     particules[*, index_min] = chains[*,length-1]
     logLikelihood_particules[index_min] = lnl[length-1]

     index_max = (WHERE(logLikelihood_particules EQ MAX(logLikelihood_particules)))[0]
     dZ = ABS(exp(-(iNest+1)/N_particules)*logLikelihood_particules[index_max])
     
     IF iNest MOD nprint EQ 0 THEN BEGIN

        IF KEYWORD_SET(save_nsstep) THEN  $
           save, FILENAME=save_prefix+'_ns.dat', iNest, startTime, $
                 logLikelihood_keep, x_keep, N_particules, fcn, particules, nested_factor, fcnargs

        IF KEYWORD_SET(verbose) THEN BEGIN
           
           NSFIT_STAT, logLikelihood_keep[0:iNest], N_particules, logMass=logMass, logZ=logZ, relWeights=relWeights,$
                       doPlot=doPlot, MSG_prefix=msg_prefix

           
           PRINT, FORMAT='(A,F4.1," / ",E7.1, " -- Accep. ratio : ", F4.2, " -- logL : ", F10.4)', msg_prefix, $
                  iNest*1.0/N_particules, dZ, N_ELEMENTS(accept)/accept[N_ELEMENTS(accept)-1], logZ
           
        ENDIF
        

     ENDIF
     
     
     iNest += 1
  ;; ENDFOR
  ENDWHILE

  ;; Only keep the good ones for the output statistics
  good_likelihood = WHERE(FINITE(logLikelihood_keep) EQ 1)
  
  NSFIT_STAT, logLikelihood_keep[good_likelihood], N_particules, logMass=logMass, logLikelihood=logLikelihood, logZ=logZ, information=information, effSampleSize=effSampleSize, relWeights=relWeights, doPlot=doPlot, doPRINT=doPRINT

  ;; Draw a posterior sample
  posterior_sample = NSFIT_POSTERIOR(x_keep[*,good_likelihood], relWeights, effSampleSize)
  
  RETURN, logZ

TERMINATE:
  RETURN, -!VALUES.D_INFINITY
  
END

